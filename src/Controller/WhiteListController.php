<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Whitelist;
use App\Form\WhitelistType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WhiteListController extends AbstractController
{
    /**
     * @Route("/douane", name="whitelist")
     */
    public function index(Request $request, ManagerRegistry $manager, UserRepository $repo)
    {
        // Appel pour récupérais l'id de l'utilisateur connecter : $id = $this->getUser()->getId();
        $id = $this->getUser()->getId();


        $whitelist = new Whitelist();
        $user = new User();
        $user = $repo->findBy(array('id' => $id));

        
        

        $form = $this->createForm(WhitelistType::class, $whitelist);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $whitelisted = 1; // 0 pas passer 1 demande effectuer 2 Demande Accepter 3 demande refuser
            // Validation des donnée à insert en bdd
            $em = $manager->getManager();
            $user[0]->setWhitelisted($whitelisted);
            $em->persist($user[0]);
            $em->flush();
        }

        if($form->isSubmitted() && $form->isValid()){
            
            $whitelisted = 1; // 0 pas passer 1 demande effectuer 2 Demande Accepter 3 demande refuser
            // Validation des donnée à insert en bdd
            $em = $manager->getManager();
            $em->persist($whitelist);
            $em->flush();
            return $this->redirectToRoute('member_profil');
        }
        return $this->render('whitelist/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
