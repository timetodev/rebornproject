<?php

namespace App\Controller;

use App\Entity\Enquete;
use App\Entity\Plainte;
use App\Entity\Rapport;
use App\Form\EnqueteType;
use App\Form\PlainteType;
use App\Form\RapportType;
use App\Entity\MetiersJoueurs;
use App\Entity\CasierJudicaire;
use App\Form\CasierJudicaireType;
use App\Repository\EnqueteRepository;
use App\Repository\PlainteRepository;
use App\Repository\RapportRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\MetiersJoueursRepository;
use App\Repository\CasierJudicaireRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PoliceController extends AbstractController
{
    /**
     * @Route("/lspd", name="lspd_home")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function index(RapportRepository $repo, MetiersJoueursRepository $repoM, EnqueteRepository $repoE, CasierJudicaireRepository $repoC, PlainteRepository $repoP)
    {
        $rapport = new Rapport();
        $rapport = $repo->findBy(array(),array('id' => 'DESC'),4);

        $enquete = new Enquete();
        $enquete = $repoE->findBy(array(),array('id' => 'DESC'),4);

        $casier = new CasierJudicaire();
        $casier = $repoC->findBy(array(),array('id' => 'DESC'),4);

        $plainte = new Plainte();
        $plainte = $repoP->findBy(array(),array('id' => 'DESC'),4);


        $steam = $this->getUser()->getSteamHex();

        $metier = new MetiersJoueurs();
        $metier = $repoM->findBy(array('identifier' => $steam));


        return $this->render('police/index.html.twig', [
            'rapports' => $rapport,
            'enquetes' => $enquete,
            'metier' => $metier[0],
            'casiers' => $casier,
            'plaintes' => $plainte,
        ]);
    }

    

      /**
     * @Route("/enqueteDelete/{id}", name="lspd_enquete_delete")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function enqueteDel(ManagerRegistry $manager,EnqueteRepository $repo,$id)
    {
        $enquete = new Enquete();
        $enqueteDel = $repo->findOneBy(array('id' => $id));
        $em = $manager->getManager();
        $em->remove($enqueteDel);
        $em->flush();
        $this->addFlash('success', 'Dossier d\'enquête supprimer avec succées!!!');
        return $this->redirectToRoute('lspd_home');
    }


    /**
     * @Route("/enqueteView/{id}", name="lspd_enquete_view")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function enqueteView(EnqueteRepository $repo, $id)
    {
        $enquete = new Enquete();
        $enquete = $repo->findOneBy(array('id' => $id));

        return $this->render('police/EnqueteView.html.twig', [
            'enquete' => $enquete,
        ]);
    }

    /**
     * @Route("/enquete", name="lspd_enquete")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function addEnquete(Request $request, ManagerRegistry $manager)
    {   
        $enquete = new Enquete();
        $form = $this->createForm(EnqueteType::class, $enquete);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
           
            $em = $manager->getManager();
            $em->persist($enquete);
            $em->flush();
            $this->addFlash('success', 'Dossier d\'enquête ajouter avec succées!!!');
            return $this->redirectToRoute('lspd_home');
        }
        return $this->render('police/AddEnquete.html.twig', [
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/enqueteAll", name="lspd_enquete_all")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function enqueteAll(enqueteRepository $repo,MetiersJoueursRepository $repoM)
    {
        $enquete = new Enquete();
        $enquete = $repo->findAll();

        $steam = $this->getUser()->getSteamHex();
        
        $metier = new MetiersJoueurs();
        $metier = $repoM->findBy(array('identifier' => $steam));

        return $this->render('police/allEnquete.html.twig', [
            'enquetes' => $enquete,
            'metier' => $metier[0],
        ]);
    }



     /**
     * @Route("/rapportDelete/{id}", name="lspd_rapport_delete")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function rapportDel(ManagerRegistry $manager,RapportRepository $repo,$id)
    {
        $rapport = new Rapport();
        $userchannel = $repo->findOneBy(array('id' => $id));
        $em = $manager->getManager();
        $em->remove($userchannel);
        $em->flush();
        $this->addFlash('success', 'Rapport supprimer avec succées!!!');
        return $this->redirectToRoute('lspd_home');
    }


     /**
     * @Route("/rapportView/{id}", name="lspd_rapport_view")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function rapportView(RapportRepository $repo, $id)
    {
        $rapport = new Rapport();
        $rapport = $repo->findOneBy(array('id' => $id));

        return $this->render('police/RapportView.html.twig', [
            'rapport' => $rapport,
        ]);
    }

    /**
     * @Route("/rapportAll", name="lspd_rapport_all")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function rapportAll(RapportRepository $repo,MetiersJoueursRepository $repoM)
    {
        $rapport = new Rapport();
        $rapport = $repo->findAll();

        $steam = $this->getUser()->getSteamHex();
        
        $metier = new MetiersJoueurs();
        $metier = $repoM->findBy(array('identifier' => $steam));

        return $this->render('police/allRapport.html.twig', [
            'rapports' => $rapport,
            'metier' => $metier[0],
        ]);
    }


     /**
     * @Route("/rapport", name="lspd_rapport")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function addRapport(Request $request, ManagerRegistry $manager)
    {   
        $rapport = new Rapport();
        $form = $this->createForm(RapportType::class, $rapport);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Hash du mot de passe et roles par default
           
            $em = $manager->getManager();
            $em->persist($rapport);
            $em->flush();
            return $this->redirectToRoute('lspd_home');
        }
        return $this->render('police/AddRapport.html.twig', [
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/casier", name="lspd_casier")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function addCasier(Request $request, ManagerRegistry $manager)
    {   
        $casier = new CasierJudicaire();
        $form = $this->createForm(CasierJudicaireType::class, $casier);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Hash du mot de passe et roles par default
           
            $em = $manager->getManager();
            $em->persist($casier);
            $em->flush();
            $this->addFlash('success', 'Casier Judiciaire Ajouter avec succées!!!');
            return $this->redirectToRoute('lspd_home');
        }
        return $this->render('police/AddCasier.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/casierViewUser/{id}", name="user_casier_view")
     */
    public function UserCasierView(CasierJudicaireRepository $repo, $id)
    {
        $user = $this->getUser()->getUsername();

        $casier = new CasierJudicaire();
        $casier = $repo->findOneBy(array('id' => $id));

        return $this->render('police/UserCasierView.html.twig', [
            'casier' => $casier,
            'username' => $user,
        ]);
    }

     /**
     * @Route("/casierView/{id}", name="lspd_casier_view")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function casierView(CasierJudicaireRepository $repo, $id)
    {
        $casier = new Enquete();
        $casier = $repo->findOneBy(array('id' => $id));

        return $this->render('police/CasierView.html.twig', [
            'casier' => $casier,
        ]);
    }
    /**
     * @Route("/casierAll", name="lspd_casier_all")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function casierAll(CasierJudicaireRepository $repo,MetiersJoueursRepository $repoM)
    {
        $casier = new Rapport();
        $casier = $repo->findAll();

        $steam = $this->getUser()->getSteamHex();
        
        $metier = new MetiersJoueurs();
        $metier = $repoM->findBy(array('identifier' => $steam));
        return $this->render('police/allCasier.html.twig', [
            'casiers' => $casier,
            'metier' => $metier[0],
        ]);
    }

         /**
     * @Route("/casierDelete/{id}", name="lspd_casier_delete")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function casierDel(ManagerRegistry $manager,CasierJudicaireRepository $repo,$id)
    {
        $casier = new CasierJudicaire();
        $userchannel = $repo->findOneBy(array('id' => $id));
        $em = $manager->getManager();
        $em->remove($userchannel);
        $em->flush();
        $this->addFlash('success', 'Casier Judiciaire supprimer avec succées!!!');
        return $this->redirectToRoute('lspd_home');
    }

          /**
     * @Route("/casierEdit/{id}", name="lspd_casier_edit")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function editPlainte(Request $request, ManagerRegistry $manager, CasierJudicaireRepository $repo,$id)
    {   
        $casier = new CasierJudicaire();
        $casier = $repo->find($id);
        $form = $this->createForm(CasierJudicaireType::class, $casier);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Hash du mot de passe et roles par default
           
            $em = $manager->getManager();
            $em->persist($casier);
            $em->flush();
            $this->addFlash('success', 'Casier Judiciaire mise à jour avec succées!!!');
            return $this->redirectToRoute('lspd_home');
        }

        return $this->render('police/EditCasier.html.twig', [
            'form' => $form->createView(),
            'casier' => $casier,
        ]);
    }

         /**
     * @Route("/plainteView/{id}", name="lspd_plainte_view")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function plainteView(PlainteRepository $repo, $id)
    {
        $plainte = new Plainte();
        $plainte = $repo->findOneBy(array('id' => $id));

        return $this->render('police/PlainteView.html.twig', [
            'plainte' => $plainte,
        ]);
    }
    /**
     * @Route("/plainteAll", name="lspd_plainte_all")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function plainteAll(plainteRepository $repo,MetiersJoueursRepository $repoM)
    {
        $plainte = new Plainte();
        $plainte = $repo->findAll();

        $steam = $this->getUser()->getSteamHex();
        
        $metier = new MetiersJoueurs();
        $metier = $repoM->findBy(array('identifier' => $steam));

        return $this->render('police/allPlainte.html.twig', [
            'plaintes' => $plainte,
            'metier' => $metier[0],
        ]);
    }

         /**
     * @Route("/plainteDelete/{id}", name="lspd_plainte_delete")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function plainteDel(ManagerRegistry $manager,PlainteRepository $repo,$id)
    {
        $plainte = new Plainte();
        $userchannel = $repo->findOneBy(array('id' => $id));
        $em = $manager->getManager();
        $em->remove($userchannel);
        $em->flush();
        return $this->redirectToRoute('lspd_home');
    }

       /**
     * @Route("/plainte", name="lspd_plainte")
     * @IsGranted("ROLE_LSPD")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD')")
     */
    public function addPlainte(Request $request, ManagerRegistry $manager)
    {   
        $plainte = new Plainte();
        $form = $this->createForm(PlainteType::class, $plainte);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Hash du mot de passe et roles par default
           
            $em = $manager->getManager();
            $em->persist($plainte);
            $em->flush();
            return $this->redirectToRoute('lspd_home');
        }
        return $this->render('police/AddPlainte.html.twig', [
            'form' => $form->createView(),
        ]);
    }




}
