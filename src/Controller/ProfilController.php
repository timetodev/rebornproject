<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\PhoneCalls;
use App\Form\UserInGameType;

use App\Entity\GestionJoueurs;

use App\Entity\CasierJudicaire;


use App\Entity\VehiculesJoueurs;
use App\Repository\UserRepository;
use App\Repository\PhoneCallsRepository;
use App\Repository\GestionJoueursRepository;
use App\Repository\CasierJudicaireRepository;
use App\Repository\TelephoneAppelsRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\VehiculesJoueursRepository;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    /**
     * @Route("/membre", name="member_profil")
     */
    public function index(UserRepository $repo, CasierJudicaireRepository $repoC, GestionJoueursRepository $repoG, PhoneCallsRepository $repoA, VehiculesJoueursRepository $repoV)
    {
        $id = $this->getUser()->getId();
        $steam = $this->getUser()->getSteamHex();
        $user = $this->getUser()->getUsername();
        $role = $this->getUser()->getRoles();
        

     

        $account = new User();
        $account = $repo->findBy(array('id' => $id));

      

       
        $joueur = new GestionJoueurs();
        $joueur = $repoG->findBy(array('identifier' => $steam));
        if(!empty($joueur)){
            $joueur = $joueur[0];
        }
        else{
            $joueur = [];
        }


        if(isset($joueur) and !empty($joueur)){
            $casier = new CasierJudicaire();
            $casier = $repoC->findBy(array('telephone' => $joueur->getTelephone()));
        }
        else{
            $casier = [];
        }
        


        if(isset($joueur) and !empty($joueur)){
        $appel = new PhoneCalls();
        $appel = $repoA->findBy(array('owner' => $joueur->getTelephone()));
        }
        else{
            $appel = [];
        }

        $veh = new VehiculesJoueurs();
        $veh = $repoV->findBy(array('identifier' => $steam));
        
      
     dump($casier);

        return $this->render('profil/index.html.twig', [
            'account' => $account,
            'joueur' => $joueur,
            'roles' => $role,
            'username' => $user,
            'phoneCalls' => $appel,
            'vehicules' => $veh,
            'casier' => $casier,
        ]);
    }

    
}
