<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Staff;
use App\Entity\Devblog;
use App\Repository\NewsRepository;
use App\Repository\StaffRepository;
use App\Repository\DevblogRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{

    /**
     * @Route("/", name="pegi_18")
     */
    public function pegi()
    {
        
        return $this->render('index/home.html.twig', [
        ]);

        
    }

    /**
     * @Route("/staff", name="page_staff")
     */
    public function staffPage(StaffRepository $repo)
    {
        $staff = new Staff();
        $staff = $repo->findAll();
        return $this->render('page_staff/index.html.twig', [
            'staffs' => $staff,
        ]);
    }


    /**
     * @Route("/home", name="index")
     */
    public function index(NewsRepository $repo, DevblogRepository $repoD)
    {
        $cache = new FilesystemAdapter();
       
        $news = new News();
        $news = $repo->findBy(array('actived' => '1'), array('id' => 'DESC'),8);

        $devblog = new Devblog();
        $devblog = $repoD->findBy(array('visible' => '1'));
        // $cache->save(array($news,$devblog));
        $productsCount = $cache->getItem('news_cache');
        $productsCount->set($devblog);
        $cache->save($productsCount);
        return $this->render('index/index.html.twig', [
            'news' => $news,
            'devblogs' => $devblog,
        ]);

        
    }


    

       /**
     * @Route("/weazel/{id}", name="news")
     */
    public function news(NewsRepository $repo, $id)
    {
   
        $news = $repo->find($id);
        // dump($news);
        return $this->render('index/news.html.twig', [
            'article' => $news,
        ]);
    }


      /**
     * @Route("/mention", name="mention_legal")
     */
    public function mention()
    {
   
       
        return $this->render('includes/mentionLegal.html.twig');
    }

     /**
     * @Route("/staff", name="staff")
     */
    public function staff()
    {
   
       
        return $this->render('includes/mentionLegal.html.twig');
    }
}
