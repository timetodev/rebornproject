<?php

namespace App\Repository;

use App\Entity\TelephoneAppels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TelephoneAppels|null find($id, $lockMode = null, $lockVersion = null)
 * @method TelephoneAppels|null findOneBy(array $criteria, array $orderBy = null)
 * @method TelephoneAppels[]    findAll()
 * @method TelephoneAppels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TelephoneAppelsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TelephoneAppels::class);
    }

    public function findByPhone($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.telephone == :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }


    // /**
    //  * @return TelephoneAppels[] Returns an array of TelephoneAppels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TelephoneAppels
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
