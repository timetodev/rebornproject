<?php

namespace App\Repository;

use App\Entity\MetiersJoueurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MetiersJoueurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method MetiersJoueurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method MetiersJoueurs[]    findAll()
 * @method MetiersJoueurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetiersJoueursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MetiersJoueurs::class);
    }

    // /**
    //  * @return MetiersJoueurs[] Returns an array of MetiersJoueurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MetiersJoueurs
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
