<?php

namespace App\Repository;

use App\Entity\VehiculesJoueurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VehiculesJoueurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehiculesJoueurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehiculesJoueurs[]    findAll()
 * @method VehiculesJoueurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehiculesJoueursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehiculesJoueurs::class);
    }

    // /**
    //  * @return VehiculesJoueurs[] Returns an array of VehiculesJoueurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VehiculesJoueurs
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
