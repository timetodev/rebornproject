<?php

namespace App\Repository;

use App\Entity\PhoneCalls;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PhoneCalls|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneCalls|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneCalls[]    findAll()
 * @method PhoneCalls[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneCallsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneCalls::class);
    }

    // /**
    //  * @return PhoneCalls[] Returns an array of PhoneCalls objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneCalls
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
