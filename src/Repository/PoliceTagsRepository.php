<?php

namespace App\Repository;

use App\Entity\PoliceTags;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PoliceTags|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoliceTags|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoliceTags[]    findAll()
 * @method PoliceTags[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoliceTagsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoliceTags::class);
    }

    // /**
    //  * @return PoliceTags[] Returns an array of PoliceTags objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PoliceTags
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
