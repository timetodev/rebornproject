<?php

namespace App\Repository;

use App\Entity\PermisJoueurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PermisJoueurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermisJoueurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermisJoueurs[]    findAll()
 * @method PermisJoueurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermisJoueursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PermisJoueurs::class);
    }

    // /**
    //  * @return PermisJoueurs[] Returns an array of PermisJoueurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PermisJoueurs
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
