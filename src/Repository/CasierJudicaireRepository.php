<?php

namespace App\Repository;

use App\Entity\CasierJudicaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CasierJudicaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method CasierJudicaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method CasierJudicaire[]    findAll()
 * @method CasierJudicaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CasierJudicaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CasierJudicaire::class);
    }

    // /**
    //  * @return CasierJudicaire[] Returns an array of CasierJudicaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CasierJudicaire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
