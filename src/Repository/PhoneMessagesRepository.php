<?php

namespace App\Repository;

use App\Entity\PhoneMessages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PhoneMessages|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneMessages|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneMessages[]    findAll()
 * @method PhoneMessages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneMessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneMessages::class);
    }

    // /**
    //  * @return PhoneMessages[] Returns an array of PhoneMessages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneMessages
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
