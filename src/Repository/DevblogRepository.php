<?php

namespace App\Repository;

use App\Entity\Devblog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Devblog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Devblog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Devblog[]    findAll()
 * @method Devblog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DevblogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Devblog::class);
    }

    // /**
    //  * @return Devblog[] Returns an array of Devblog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Devblog
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
