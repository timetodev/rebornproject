<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200112141021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user ADD whitelisted TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE user DROP whitelisted');
    }
}
