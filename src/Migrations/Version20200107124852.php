<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200107124852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL, DROP rank');
        $this->addSql('ALTER TABLE vehicules_joueurs CHANGE id id INT NOT NULL, CHANGE donnees donnees VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE vehicules_professionnel CHANGE id id INT NOT NULL, CHANGE donnees donnees VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE user ADD rank LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', DROP roles');
        $this->addSql('ALTER TABLE vehicules_joueurs CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE donnees donnees VARCHAR(2000) CHARACTER SET utf8 DEFAULT \'{"vehicule_windowtint":-1,"vehicule_variation":"off","vehicule_neoncolor3":255,"vehicule_tiresmoke":"off","vehicule_smokecolor1":255,"vehicule_smokecolor2":255,"vehicule_smokecolor3":255,"vehicule_neon0":"off","vehicule_pearlescentcolor":90,"vehicule_neon1":"off","vehicule_bulletproof":"off","vehicule_secondarycolor":0,"vehicule_neon3":"off","vehicule_wheelcolor":156,"vehicule_neoncolor2":0,"vehicule_neoncolor1":255,"vehicule_wheeltype":4,"vehicule_xenon":"off","vehicule_primarycolor":0,"vehicule_plateindex":0,"vehicule_mods":{"1":-1,"2":-1,"3":-1,"4":-1,"5":0,"6":0,"7":-1,"8":0,"9":0,"10":0,"11":-1,"12":-1,"13":-1,"14":-1,"15":0,"16":-1,"17":0,"18":0,"19":0,"20":0,"21":-1,"22":0,"23":-1,"24":0,"25":0,"26":0,"27":0,"28":0,"29":0,"30":0,"31":0,"32":0,"33":0,"34":0,"35":0,"36":0,"37":0,"38":0,"39":0,"40":0,"41":0,"42":0,"43":0,"44":0,"45":0,"46":0,"47":0,"48":0,"0":0},"vehicule_turbo":"off","vehicule_neon2":"off"}\' NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE vehicules_professionnel CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE donnees donnees VARCHAR(2000) CHARACTER SET utf8 DEFAULT \'{"vehicule_windowtint":-1,"vehicule_variation":"off","vehicule_neoncolor3":255,"vehicule_tiresmoke":"off","vehicule_smokecolor1":255,"vehicule_smokecolor2":255,"vehicule_smokecolor3":255,"vehicule_neon0":"off","vehicule_pearlescentcolor":90,"vehicule_neon1":"off","vehicule_bulletproof":"off","vehicule_secondarycolor":0,"vehicule_neon3":"off","vehicule_wheelcolor":156,"vehicule_neoncolor2":0,"vehicule_neoncolor1":255,"vehicule_wheeltype":4,"vehicule_xenon":"off","vehicule_primarycolor":0,"vehicule_plateindex":0,"vehicule_mods":{"1":-1,"2":-1,"3":-1,"4":-1,"5":0,"6":0,"7":-1,"8":0,"9":0,"10":0,"11":-1,"12":-1,"13":-1,"14":-1,"15":0,"16":-1,"17":0,"18":0,"19":0,"20":0,"21":-1,"22":0,"23":-1,"24":0,"25":0,"26":0,"27":0,"28":0,"29":0,"30":0,"31":0,"32":0,"33":0,"34":0,"35":0,"36":0,"37":0,"38":0,"39":0,"40":0,"41":0,"42":0,"43":0,"44":0,"45":0,"46":0,"47":0,"48":0,"0":0},"vehicule_turbo":"off","vehicule_neon2":"off"}\' NOT NULL COLLATE `utf8_general_ci`');
    }
}
