<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200111134551 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE whitelist (id INT AUTO_INCREMENT NOT NULL, steam_id VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, q1 VARCHAR(255) NOT NULL, q2 VARCHAR(255) NOT NULL, q3 VARCHAR(255) NOT NULL, q4 VARCHAR(255) NOT NULL, q5 VARCHAR(255) NOT NULL, background LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE permis_joueurs CHANGE identifier identifier VARCHAR(255) NOT NULL, CHANGE pointsvoiture pointsvoiture INT NOT NULL, CHANGE moto moto INT NOT NULL, CHANGE camion camion INT NOT NULL, CHANGE armes armes INT NOT NULL, CHANGE helicoptere helicoptere INT NOT NULL, CHANGE avion avion INT NOT NULL, CHANGE bateau bateau INT NOT NULL, CHANGE peche peche INT NOT NULL, CHANGE chasse chasse INT NOT NULL, CHANGE detective detective INT NOT NULL');
        $this->addSql('ALTER TABLE vehicules_joueurs CHANGE id id INT NOT NULL, CHANGE donnees donnees VARCHAR(2000) NOT NULL');
        $this->addSql('ALTER TABLE vehicules_professionnel CHANGE id id INT NOT NULL, CHANGE donnees donnees VARCHAR(2000) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE whitelist');
        $this->addSql('ALTER TABLE coffre_fort_entreprise CHANGE id id VARCHAR(25) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE gestion_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE metiers_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE pareballe_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE permis_joueurs CHANGE identifier identifier VARCHAR(255) CHARACTER SET latin1 DEFAULT \'0\' NOT NULL COLLATE `latin1_swedish_ci`, CHANGE pointsvoiture pointsvoiture INT DEFAULT 0 NOT NULL, CHANGE moto moto INT DEFAULT 0 NOT NULL, CHANGE camion camion INT DEFAULT 0 NOT NULL, CHANGE armes armes INT DEFAULT 0 NOT NULL, CHANGE helicoptere helicoptere INT DEFAULT 0 NOT NULL, CHANGE avion avion INT DEFAULT 0 NOT NULL, CHANGE bateau bateau INT DEFAULT 0 NOT NULL, CHANGE peche peche INT DEFAULT 0 NOT NULL, CHANGE chasse chasse INT DEFAULT 0 NOT NULL, CHANGE detective detective INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE vehicules_joueurs CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE donnees donnees VARCHAR(2000) CHARACTER SET utf8 DEFAULT \'{"vehicule_windowtint":-1,"vehicule_variation":"off","vehicule_neoncolor3":255,"vehicule_tiresmoke":"off","vehicule_smokecolor1":255,"vehicule_smokecolor2":255,"vehicule_smokecolor3":255,"vehicule_neon0":"off","vehicule_pearlescentcolor":90,"vehicule_neon1":"off","vehicule_bulletproof":"off","vehicule_secondarycolor":0,"vehicule_neon3":"off","vehicule_wheelcolor":156,"vehicule_neoncolor2":0,"vehicule_neoncolor1":255,"vehicule_wheeltype":4,"vehicule_xenon":"off","vehicule_primarycolor":0,"vehicule_plateindex":0,"vehicule_mods":{"1":-1,"2":-1,"3":-1,"4":-1,"5":0,"6":0,"7":-1,"8":0,"9":0,"10":0,"11":-1,"12":-1,"13":-1,"14":-1,"15":0,"16":-1,"17":0,"18":0,"19":0,"20":0,"21":-1,"22":0,"23":-1,"24":0,"25":0,"26":0,"27":0,"28":0,"29":0,"30":0,"31":0,"32":0,"33":0,"34":0,"35":0,"36":0,"37":0,"38":0,"39":0,"40":0,"41":0,"42":0,"43":0,"44":0,"45":0,"46":0,"47":0,"48":0,"0":0},"vehicule_turbo":"off","vehicule_neon2":"off"}\' NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE vehicules_professionnel CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE donnees donnees VARCHAR(2000) CHARACTER SET utf8 DEFAULT \'{"vehicule_windowtint":-1,"vehicule_variation":"off","vehicule_neoncolor3":255,"vehicule_tiresmoke":"off","vehicule_smokecolor1":255,"vehicule_smokecolor2":255,"vehicule_smokecolor3":255,"vehicule_neon0":"off","vehicule_pearlescentcolor":90,"vehicule_neon1":"off","vehicule_bulletproof":"off","vehicule_secondarycolor":0,"vehicule_neon3":"off","vehicule_wheelcolor":156,"vehicule_neoncolor2":0,"vehicule_neoncolor1":255,"vehicule_wheeltype":4,"vehicule_xenon":"off","vehicule_primarycolor":0,"vehicule_plateindex":0,"vehicule_mods":{"1":-1,"2":-1,"3":-1,"4":-1,"5":0,"6":0,"7":-1,"8":0,"9":0,"10":0,"11":-1,"12":-1,"13":-1,"14":-1,"15":0,"16":-1,"17":0,"18":0,"19":0,"20":0,"21":-1,"22":0,"23":-1,"24":0,"25":0,"26":0,"27":0,"28":0,"29":0,"30":0,"31":0,"32":0,"33":0,"34":0,"35":0,"36":0,"37":0,"38":0,"39":0,"40":0,"41":0,"42":0,"43":0,"44":0,"45":0,"46":0,"47":0,"48":0,"0":0},"vehicule_turbo":"off","vehicule_neon2":"off"}\' NOT NULL COLLATE `utf8_general_ci`');
    }
}
