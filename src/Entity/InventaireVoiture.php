<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventaireVoiture
 *
 * @ORM\Table(name="inventaire_voiture")
 * @ORM\Entity
 */
class InventaireVoiture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="joueur", type="string", length=255, nullable=false)
     */
    private $joueur;

    /**
     * @var string
     *
     * @ORM\Column(name="plaque", type="string", length=50, nullable=false)
     */
    private $plaque;

    /**
     * @var int
     *
     * @ORM\Column(name="item", type="integer", nullable=false)
     */
    private $item;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoueur(): ?string
    {
        return $this->joueur;
    }

    public function setJoueur(string $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getPlaque(): ?string
    {
        return $this->plaque;
    }

    public function setPlaque(string $plaque): self
    {
        $this->plaque = $plaque;

        return $this;
    }

    public function getItem(): ?int
    {
        return $this->item;
    }

    public function setItem(int $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
