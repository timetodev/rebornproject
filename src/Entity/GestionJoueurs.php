<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GestionJoueurs
 *
 * @ORM\Table(name="gestion_joueurs")
 * @ORM\Entity(repositoryClass="App\Repository\GestionJoueursRepository")
 */
class GestionJoueurs
{
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $identifier;

    /**
     * @var int
     *
     * @ORM\Column(name="admin", type="integer", nullable=false)
     */
    private $admin;

    /**
     * @var int|null
     *
     * @ORM\Column(name="argent", type="integer", nullable=true)
     */
    private $argent;

    /**
     * @var int|null
     *
     * @ORM\Column(name="banque", type="integer", nullable=true)
     */
    private $banque;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dirtymoney", type="integer", nullable=true)
     */
    private $dirtymoney;

    /**
     * @var int
     *
     * @ORM\Column(name="job", type="integer", nullable=false, options={"default"="1"})
     */
    private $job = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false, options={"default"="Citizen"})
     */
    private $nom = 'Citizen';

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=false, options={"default"="Citizen"})
     */
    private $prenom = 'Citizen';

    /**
     * @var int
     *
     * @ORM\Column(name="sante", type="integer", nullable=false, options={"default"="200"})
     */
    private $sante = '200';

    /**
     * @var int
     *
     * @ORM\Column(name="eau", type="integer", nullable=false, options={"default"="100"})
     */
    private $eau = '100';

    /**
     * @var int
     *
     * @ORM\Column(name="pain", type="integer", nullable=false, options={"default"="100"})
     */
    private $pain = '100';

    /**
     * @var int
     *
     * @ORM\Column(name="alcool", type="integer", nullable=false)
     */
    private $alcool;

    /**
     * @var int
     *
     * @ORM\Column(name="enService", type="integer", nullable=false)
     */
    private $enservice;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=30, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="lastposition", type="string", length=255, nullable=false)
     */
    private $lastposition;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=false, options={"default"="f"})
     */
    private $sexe = 'f';

    /**
     * @var int
     *
     * @ORM\Column(name="taille", type="integer", nullable=false, options={"default"="190"})
     */
    private $taille = '190';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateNaissance", type="date", nullable=false, options={"default"="2010-04-06"})
     */
    private $datenaissance = '2010-04-06';

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getAdmin(): ?int
    {
        return $this->admin;
    }

    public function setAdmin(int $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getArgent(): ?int
    {
        return $this->argent;
    }

    public function setArgent(?int $argent): self
    {
        $this->argent = $argent;

        return $this;
    }

    public function getBanque(): ?int
    {
        return $this->banque;
    }

    public function setBanque(?int $banque): self
    {
        $this->banque = $banque;

        return $this;
    }

    public function getDirtymoney(): ?int
    {
        return $this->dirtymoney;
    }

    public function setDirtymoney(?int $dirtymoney): self
    {
        $this->dirtymoney = $dirtymoney;

        return $this;
    }

    public function getJob(): ?int
    {
        return $this->job;
    }

    public function setJob(int $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSante(): ?int
    {
        return $this->sante;
    }

    public function setSante(int $sante): self
    {
        $this->sante = $sante;

        return $this;
    }

    public function getEau(): ?int
    {
        return $this->eau;
    }

    public function setEau(int $eau): self
    {
        $this->eau = $eau;

        return $this;
    }

    public function getPain(): ?int
    {
        return $this->pain;
    }

    public function setPain(int $pain): self
    {
        $this->pain = $pain;

        return $this;
    }

    public function getAlcool(): ?int
    {
        return $this->alcool;
    }

    public function setAlcool(int $alcool): self
    {
        $this->alcool = $alcool;

        return $this;
    }

    public function getEnservice(): ?int
    {
        return $this->enservice;
    }

    public function setEnservice(int $enservice): self
    {
        $this->enservice = $enservice;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getLastposition(): ?string
    {
        return $this->lastposition;
    }

    public function setLastposition(string $lastposition): self
    {
        $this->lastposition = $lastposition;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getTaille(): ?int
    {
        return $this->taille;
    }

    public function setTaille(int $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getDatenaissance(): ?\DateTimeInterface
    {
        return $this->datenaissance;
    }

    public function setDatenaissance(\DateTimeInterface $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }


}
