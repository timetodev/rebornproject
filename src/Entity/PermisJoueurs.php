<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermisJoueurs
 *
 * @ORM\Table(name="permis_joueurs")
 * @ORM\Entity
 */
class PermisJoueurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="voiture", type="string", length=55, nullable=false, options={"default"="Requis"})
     */
    private $voiture = 'Requis';

    /**
     * @var int
     *
     * @ORM\Column(name="pointsvoiture", type="integer", nullable=false)
     */
    private $pointsvoiture;

    /**
     * @var string
     *
     * @ORM\Column(name="datevoiture", type="string", length=255, nullable=false, options={"default"="01/01/2020"})
     */
    private $datevoiture = '01/01/2020';

    /**
     * @var int
     *
     * @ORM\Column(name="moto", type="integer", nullable=false)
     */
    private $moto;

    /**
     * @var int
     *
     * @ORM\Column(name="camion", type="integer", nullable=false)
     */
    private $camion;

    /**
     * @var int
     *
     * @ORM\Column(name="armes", type="integer", nullable=false)
     */
    private $armes;

    /**
     * @var int
     *
     * @ORM\Column(name="helicoptere", type="integer", nullable=false)
     */
    private $helicoptere;

    /**
     * @var int
     *
     * @ORM\Column(name="avion", type="integer", nullable=false)
     */
    private $avion;

    /**
     * @var int
     *
     * @ORM\Column(name="bateau", type="integer", nullable=false)
     */
    private $bateau;

    /**
     * @var int
     *
     * @ORM\Column(name="peche", type="integer", nullable=false)
     */
    private $peche;

    /**
     * @var int
     *
     * @ORM\Column(name="chasse", type="integer", nullable=false)
     */
    private $chasse;

    /**
     * @var int
     *
     * @ORM\Column(name="detective", type="integer", nullable=false)
     */
    private $detective;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getVoiture(): ?string
    {
        return $this->voiture;
    }

    public function setVoiture(string $voiture): self
    {
        $this->voiture = $voiture;

        return $this;
    }

    public function getPointsvoiture(): ?int
    {
        return $this->pointsvoiture;
    }

    public function setPointsvoiture(int $pointsvoiture): self
    {
        $this->pointsvoiture = $pointsvoiture;

        return $this;
    }

    public function getDatevoiture(): ?string
    {
        return $this->datevoiture;
    }

    public function setDatevoiture(string $datevoiture): self
    {
        $this->datevoiture = $datevoiture;

        return $this;
    }

    public function getMoto(): ?int
    {
        return $this->moto;
    }

    public function setMoto(int $moto): self
    {
        $this->moto = $moto;

        return $this;
    }

    public function getCamion(): ?int
    {
        return $this->camion;
    }

    public function setCamion(int $camion): self
    {
        $this->camion = $camion;

        return $this;
    }

    public function getArmes(): ?int
    {
        return $this->armes;
    }

    public function setArmes(int $armes): self
    {
        $this->armes = $armes;

        return $this;
    }

    public function getHelicoptere(): ?int
    {
        return $this->helicoptere;
    }

    public function setHelicoptere(int $helicoptere): self
    {
        $this->helicoptere = $helicoptere;

        return $this;
    }

    public function getAvion(): ?int
    {
        return $this->avion;
    }

    public function setAvion(int $avion): self
    {
        $this->avion = $avion;

        return $this;
    }

    public function getBateau(): ?int
    {
        return $this->bateau;
    }

    public function setBateau(int $bateau): self
    {
        $this->bateau = $bateau;

        return $this;
    }

    public function getPeche(): ?int
    {
        return $this->peche;
    }

    public function setPeche(int $peche): self
    {
        $this->peche = $peche;

        return $this;
    }

    public function getChasse(): ?int
    {
        return $this->chasse;
    }

    public function setChasse(int $chasse): self
    {
        $this->chasse = $chasse;

        return $this;
    }

    public function getDetective(): ?int
    {
        return $this->detective;
    }

    public function setDetective(int $detective): self
    {
        $this->detective = $detective;

        return $this;
    }


}
