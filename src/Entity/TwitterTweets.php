<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TwitterTweets
 *
 * @ORM\Table(name="twitter_tweets", indexes={@ORM\Index(name="FK_twitter_tweets_twitter_accounts", columns={"authorId"})})
 * @ORM\Entity
 */
class TwitterTweets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="realUser", type="string", length=50, nullable=true)
     */
    private $realuser;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=256, nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $time = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="likes", type="integer", nullable=false)
     */
    private $likes;

    /**
     * @var \TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="TwitterAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="authorId", referencedColumnName="id")
     * })
     */
    private $authorid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRealuser(): ?string
    {
        return $this->realuser;
    }

    public function setRealuser(?string $realuser): self
    {
        $this->realuser = $realuser;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getLikes(): ?int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getAuthorid(): ?TwitterAccounts
    {
        return $this->authorid;
    }

    public function setAuthorid(?TwitterAccounts $authorid): self
    {
        $this->authorid = $authorid;

        return $this;
    }


}
