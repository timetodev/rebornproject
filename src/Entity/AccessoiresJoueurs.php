<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessoiresJoueurs
 *
 * @ORM\Table(name="accessoires_joueurs")
 * @ORM\Entity
 */
class AccessoiresJoueurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier;

    /**
     * @var int
     *
     * @ORM\Column(name="prop_id", type="integer", nullable=false)
     */
    private $propId;

    /**
     * @var int
     *
     * @ORM\Column(name="prop_text", type="integer", nullable=false)
     */
    private $propText;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=false)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="porter", type="string", length=255, nullable=false)
     */
    private $porter;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getPropId(): ?int
    {
        return $this->propId;
    }

    public function setPropId(int $propId): self
    {
        $this->propId = $propId;

        return $this;
    }

    public function getPropText(): ?int
    {
        return $this->propText;
    }

    public function setPropText(int $propText): self
    {
        $this->propText = $propText;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getPorter(): ?string
    {
        return $this->porter;
    }

    public function setPorter(string $porter): self
    {
        $this->porter = $porter;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
