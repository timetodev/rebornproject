<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VehiculesJoueurs
 *
 * @ORM\Table(name="vehicules_joueurs", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity(repositoryClass="App\Repository\VehiculesJoueursRepository")
 */
class VehiculesJoueurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $identifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_name", type="string", length=60, nullable=true)
     */
    private $vehicleName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_model", type="string", length=60, nullable=true)
     */
    private $vehicleModel;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_price", type="integer", nullable=true)
     */
    private $vehiclePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicle_plate", type="string", length=60, nullable=false)
     */
    private $vehiclePlate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_state", type="string", length=60, nullable=true)
     */
    private $vehicleState;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicle_fourriere", type="string", length=255, nullable=false, options={"default"="Non"})
     */
    private $vehicleFourriere = 'Non';

    /**
     * @var string
     *
     * @ORM\Column(name="vehicle_voler", type="string", length=255, nullable=false, options={"default"="Non"})
     */
    private $vehicleVoler = 'Non';

    /**
     * @var string
     *
     * @ORM\Column(name="santemot", type="string", length=255, nullable=false, options={"default"="998.2545166015625"})
     */
    private $santemot = '998.2545166015625';

    /**
     * @var string
     *
     * @ORM\Column(name="santecarosserie", type="string", length=255, nullable=false, options={"default"="998.2545166015625"})
     */
    private $santecarosserie = '998.2545166015625';

    /**
     * @var string
     *
     * @ORM\Column(name="essence", type="string", length=255, nullable=false, options={"default"="99.503999710083008"})
     */
    private $essence = '99.503999710083008';

    /**
     * @var string
     *
     * @ORM\Column(name="nitro", type="string", length=255, nullable=false, options={"default"="Non"})
     */
    private $nitro = 'Non';

    /**
     * @var string
     *
     * @ORM\Column(name="donnees", type="string", length=2000, nullable=false)
     */
    private $donnees;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getVehicleName(): ?string
    {
        return $this->vehicleName;
    }

    public function setVehicleName(?string $vehicleName): self
    {
        $this->vehicleName = $vehicleName;

        return $this;
    }

    public function getVehicleModel(): ?string
    {
        return $this->vehicleModel;
    }

    public function setVehicleModel(?string $vehicleModel): self
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }

    public function getVehiclePrice(): ?int
    {
        return $this->vehiclePrice;
    }

    public function setVehiclePrice(?int $vehiclePrice): self
    {
        $this->vehiclePrice = $vehiclePrice;

        return $this;
    }

    public function getVehiclePlate(): ?string
    {
        return $this->vehiclePlate;
    }

    public function setVehiclePlate(string $vehiclePlate): self
    {
        $this->vehiclePlate = $vehiclePlate;

        return $this;
    }

    public function getVehicleState(): ?string
    {
        return $this->vehicleState;
    }

    public function setVehicleState(?string $vehicleState): self
    {
        $this->vehicleState = $vehicleState;

        return $this;
    }

    public function getVehicleFourriere(): ?string
    {
        return $this->vehicleFourriere;
    }

    public function setVehicleFourriere(string $vehicleFourriere): self
    {
        $this->vehicleFourriere = $vehicleFourriere;

        return $this;
    }

    public function getVehicleVoler(): ?string
    {
        return $this->vehicleVoler;
    }

    public function setVehicleVoler(string $vehicleVoler): self
    {
        $this->vehicleVoler = $vehicleVoler;

        return $this;
    }

    public function getSantemot(): ?string
    {
        return $this->santemot;
    }

    public function setSantemot(string $santemot): self
    {
        $this->santemot = $santemot;

        return $this;
    }

    public function getSantecarosserie(): ?string
    {
        return $this->santecarosserie;
    }

    public function setSantecarosserie(string $santecarosserie): self
    {
        $this->santecarosserie = $santecarosserie;

        return $this;
    }

    public function getEssence(): ?string
    {
        return $this->essence;
    }

    public function setEssence(string $essence): self
    {
        $this->essence = $essence;

        return $this;
    }

    public function getNitro(): ?string
    {
        return $this->nitro;
    }

    public function setNitro(string $nitro): self
    {
        $this->nitro = $nitro;

        return $this;
    }

    public function getDonnees(): ?string
    {
        return $this->donnees;
    }

    public function setDonnees(string $donnees): self
    {
        $this->donnees = $donnees;

        return $this;
    }


}
