<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skin
 *
 * @ORM\Table(name="skin")
 * @ORM\Entity
 */
class Skin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identifier", type="string", length=120, nullable=true)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="donnees", type="string", length=2000, nullable=false)
     */
    private $donnees;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getDonnees(): ?string
    {
        return $this->donnees;
    }

    public function setDonnees(string $donnees): self
    {
        $this->donnees = $donnees;

        return $this;
    }


}
