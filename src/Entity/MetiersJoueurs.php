<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MetiersJoueurs
 *
 * @ORM\Table(name="metiers_joueurs")
 * @ORM\Entity(repositoryClass="App\Repository\MetiersJoueursRepository")
 */
class MetiersJoueurs
{
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $identifier;

    /**
     * @var int
     *
     * @ORM\Column(name="Police", type="integer", nullable=false)
     */
    private $police;

    /**
     * @var int
     *
     * @ORM\Column(name="Taxi", type="integer", nullable=false)
     */
    private $taxi;

    /**
     * @var int
     *
     * @ORM\Column(name="Medecin", type="integer", nullable=false)
     */
    private $medecin;

    /**
     * @var int
     *
     * @ORM\Column(name="Depanneur", type="integer", nullable=false)
     */
    private $depanneur;

    /**
     * @var int
     *
     * @ORM\Column(name="Bennys", type="integer", nullable=false)
     */
    private $bennys;

    /**
     * @var int
     *
     * @ORM\Column(name="Epicier", type="integer", nullable=false)
     */
    private $epicier;

    /**
     * @var int
     *
     * @ORM\Column(name="Gangster", type="integer", nullable=false)
     */
    private $gangster;

    /**
     * @var int
     *
     * @ORM\Column(name="Tatoueur", type="integer", nullable=false)
     */
    private $tatoueur;

    /**
     * @var int
     *
     * @ORM\Column(name="PDM", type="integer", nullable=false)
     */
    private $pdm;

    /**
     * @var int
     *
     * @ORM\Column(name="GarageNOS", type="integer", nullable=false)
     */
    private $garagenos;

    /**
     * @var int
     *
     * @ORM\Column(name="Immobilier", type="integer", nullable=false)
     */
    private $immobilier;

    /**
     * @var int
     *
     * @ORM\Column(name="GangRouge", type="integer", nullable=false)
     */
    private $gangrouge;

    /**
     * @var int
     *
     * @ORM\Column(name="GangViolet", type="integer", nullable=false)
     */
    private $gangviolet;

    /**
     * @var int
     *
     * @ORM\Column(name="GangVert", type="integer", nullable=false)
     */
    private $gangvert;

    /**
     * @var int
     *
     * @ORM\Column(name="GangJaune", type="integer", nullable=false)
     */
    private $gangjaune;

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getPolice(): ?int
    {
        return $this->police;
    }

    public function setPolice(int $police): self
    {
        $this->police = $police;

        return $this;
    }

    public function getTaxi(): ?int
    {
        return $this->taxi;
    }

    public function setTaxi(int $taxi): self
    {
        $this->taxi = $taxi;

        return $this;
    }

    public function getMedecin(): ?int
    {
        return $this->medecin;
    }

    public function setMedecin(int $medecin): self
    {
        $this->medecin = $medecin;

        return $this;
    }

    public function getDepanneur(): ?int
    {
        return $this->depanneur;
    }

    public function setDepanneur(int $depanneur): self
    {
        $this->depanneur = $depanneur;

        return $this;
    }

    public function getBennys(): ?int
    {
        return $this->bennys;
    }

    public function setBennys(int $bennys): self
    {
        $this->bennys = $bennys;

        return $this;
    }

    public function getEpicier(): ?int
    {
        return $this->epicier;
    }

    public function setEpicier(int $epicier): self
    {
        $this->epicier = $epicier;

        return $this;
    }

    public function getGangster(): ?int
    {
        return $this->gangster;
    }

    public function setGangster(int $gangster): self
    {
        $this->gangster = $gangster;

        return $this;
    }

    public function getTatoueur(): ?int
    {
        return $this->tatoueur;
    }

    public function setTatoueur(int $tatoueur): self
    {
        $this->tatoueur = $tatoueur;

        return $this;
    }

    public function getPdm(): ?int
    {
        return $this->pdm;
    }

    public function setPdm(int $pdm): self
    {
        $this->pdm = $pdm;

        return $this;
    }

    public function getGaragenos(): ?int
    {
        return $this->garagenos;
    }

    public function setGaragenos(int $garagenos): self
    {
        $this->garagenos = $garagenos;

        return $this;
    }

    public function getImmobilier(): ?int
    {
        return $this->immobilier;
    }

    public function setImmobilier(int $immobilier): self
    {
        $this->immobilier = $immobilier;

        return $this;
    }

    public function getGangrouge(): ?int
    {
        return $this->gangrouge;
    }

    public function setGangrouge(int $gangrouge): self
    {
        $this->gangrouge = $gangrouge;

        return $this;
    }

    public function getGangviolet(): ?int
    {
        return $this->gangviolet;
    }

    public function setGangviolet(int $gangviolet): self
    {
        $this->gangviolet = $gangviolet;

        return $this;
    }

    public function getGangvert(): ?int
    {
        return $this->gangvert;
    }

    public function setGangvert(int $gangvert): self
    {
        $this->gangvert = $gangvert;

        return $this;
    }

    public function getGangjaune(): ?int
    {
        return $this->gangjaune;
    }

    public function setGangjaune(int $gangjaune): self
    {
        $this->gangjaune = $gangjaune;

        return $this;
    }


}
