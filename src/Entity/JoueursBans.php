<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JoueursBans
 *
 * @ORM\Table(name="joueurs_bans")
 * @ORM\Entity
 */
class JoueursBans
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false, options={"default"="Citizen"})
     */
    private $nom = 'Citizen';

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255, nullable=false, options={"default"="2010-04-06"})
     */
    private $date = '2010-04-06';

    /**
     * @var string
     *
     * @ORM\Column(name="heure", type="string", length=255, nullable=false, options={"default"="12:05:07"})
     */
    private $heure = '12:05:07';

    /**
     * @var string
     *
     * @ORM\Column(name="verif", type="string", length=255, nullable=false, options={"default"="oui"})
     */
    private $verif = 'oui';

    /**
     * @var string
     *
     * @ORM\Column(name="bandef", type="string", length=255, nullable=false, options={"default"="Non"})
     */
    private $bandef = 'Non';

    /**
     * @var string
     *
     * @ORM\Column(name="raison", type="string", length=2000, nullable=false)
     */
    private $raison;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHeure(): ?string
    {
        return $this->heure;
    }

    public function setHeure(string $heure): self
    {
        $this->heure = $heure;

        return $this;
    }

    public function getVerif(): ?string
    {
        return $this->verif;
    }

    public function setVerif(string $verif): self
    {
        $this->verif = $verif;

        return $this;
    }

    public function getBandef(): ?string
    {
        return $this->bandef;
    }

    public function setBandef(string $bandef): self
    {
        $this->bandef = $bandef;

        return $this;
    }

    public function getRaison(): ?string
    {
        return $this->raison;
    }

    public function setRaison(string $raison): self
    {
        $this->raison = $raison;

        return $this;
    }


}
