<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppartementsJoueur
 *
 * @ORM\Table(name="appartements_joueur")
 * @ORM\Entity
 */
class AppartementsJoueur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=60, nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="xe", type="string", length=255, nullable=false)
     */
    private $xe;

    /**
     * @var string
     *
     * @ORM\Column(name="ye", type="string", length=255, nullable=false)
     */
    private $ye;

    /**
     * @var string
     *
     * @ORM\Column(name="ze", type="string", length=255, nullable=false)
     */
    private $ze;

    /**
     * @var string
     *
     * @ORM\Column(name="xo", type="string", length=255, nullable=false)
     */
    private $xo;

    /**
     * @var string
     *
     * @ORM\Column(name="yo", type="string", length=255, nullable=false)
     */
    private $yo;

    /**
     * @var string
     *
     * @ORM\Column(name="zo", type="string", length=255, nullable=false)
     */
    private $zo;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false, options={"default"="Chambre"})
     */
    private $nom = 'Chambre';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getXe(): ?string
    {
        return $this->xe;
    }

    public function setXe(string $xe): self
    {
        $this->xe = $xe;

        return $this;
    }

    public function getYe(): ?string
    {
        return $this->ye;
    }

    public function setYe(string $ye): self
    {
        $this->ye = $ye;

        return $this;
    }

    public function getZe(): ?string
    {
        return $this->ze;
    }

    public function setZe(string $ze): self
    {
        $this->ze = $ze;

        return $this;
    }

    public function getXo(): ?string
    {
        return $this->xo;
    }

    public function setXo(string $xo): self
    {
        $this->xo = $xo;

        return $this;
    }

    public function getYo(): ?string
    {
        return $this->yo;
    }

    public function setYo(string $yo): self
    {
        $this->yo = $yo;

        return $this;
    }

    public function getZo(): ?string
    {
        return $this->zo;
    }

    public function setZo(string $zo): self
    {
        $this->zo = $zo;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
