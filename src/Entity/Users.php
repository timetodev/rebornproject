<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class Users
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=30, nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="group", type="string", length=50, nullable=false)
     */
    private $group;

    /**
     * @var int
     *
     * @ORM\Column(name="permission_level", type="integer", nullable=false)
     */
    private $permissionLevel;

    /**
     * @var float
     *
     * @ORM\Column(name="money", type="float", precision=10, scale=0, nullable=false)
     */
    private $money;

    /**
     * @var int
     *
     * @ORM\Column(name="bank", type="integer", nullable=false)
     */
    private $bank;

    /**
     * @var int
     *
     * @ORM\Column(name="isFirstConnection", type="integer", nullable=false, options={"default"="1"})
     */
    private $isfirstconnection = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="license", type="string", length=50, nullable=true)
     */
    private $license;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getGroup(): ?string
    {
        return $this->group;
    }

    public function setGroup(string $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getPermissionLevel(): ?int
    {
        return $this->permissionLevel;
    }

    public function setPermissionLevel(int $permissionLevel): self
    {
        $this->permissionLevel = $permissionLevel;

        return $this;
    }

    public function getMoney(): ?float
    {
        return $this->money;
    }

    public function setMoney(float $money): self
    {
        $this->money = $money;

        return $this;
    }

    public function getBank(): ?int
    {
        return $this->bank;
    }

    public function setBank(int $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getIsfirstconnection(): ?int
    {
        return $this->isfirstconnection;
    }

    public function setIsfirstconnection(int $isfirstconnection): self
    {
        $this->isfirstconnection = $isfirstconnection;

        return $this;
    }

    public function getLicense(): ?string
    {
        return $this->license;
    }

    public function setLicense(?string $license): self
    {
        $this->license = $license;

        return $this;
    }


}
