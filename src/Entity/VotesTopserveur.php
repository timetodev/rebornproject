<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VotesTopserveur
 *
 * @ORM\Table(name="votes_topserveur")
 * @ORM\Entity
 */
class VotesTopserveur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false, options={"default"="Citizen Citizen"})
     */
    private $nom = 'Citizen Citizen';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false, options={"default"="127.0.0.1"})
     */
    private $ip = '127.0.0.1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false, options={"default"="2010-04-06"})
     */
    private $date = '2010-04-06';

    /**
     * @var int
     *
     * @ORM\Column(name="nombredevotes", type="integer", nullable=false, options={"default"="1"})
     */
    private $nombredevotes = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="recuperation", type="string", length=55, nullable=false, options={"default"="active"})
     */
    private $recuperation = 'active';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNombredevotes(): ?int
    {
        return $this->nombredevotes;
    }

    public function setNombredevotes(int $nombredevotes): self
    {
        $this->nombredevotes = $nombredevotes;

        return $this;
    }

    public function getRecuperation(): ?string
    {
        return $this->recuperation;
    }

    public function setRecuperation(string $recuperation): self
    {
        $this->recuperation = $recuperation;

        return $this;
    }


}
