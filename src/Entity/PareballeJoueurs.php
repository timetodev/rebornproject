<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PareballeJoueurs
 *
 * @ORM\Table(name="pareballe_joueurs")
 * @ORM\Entity
 */
class PareballeJoueurs
{
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $identifier;

    /**
     * @var int
     *
     * @ORM\Column(name="type_pareballe", type="integer", nullable=false)
     */
    private $typePareballe;

    /**
     * @var int
     *
     * @ORM\Column(name="value_pareballe", type="integer", nullable=false)
     */
    private $valuePareballe;

    /**
     * @var string
     *
     * @ORM\Column(name="porter", type="string", length=255, nullable=false, options={"default"="non"})
     */
    private $porter = 'non';

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getTypePareballe(): ?int
    {
        return $this->typePareballe;
    }

    public function setTypePareballe(int $typePareballe): self
    {
        $this->typePareballe = $typePareballe;

        return $this;
    }

    public function getValuePareballe(): ?int
    {
        return $this->valuePareballe;
    }

    public function setValuePareballe(int $valuePareballe): self
    {
        $this->valuePareballe = $valuePareballe;

        return $this;
    }

    public function getPorter(): ?string
    {
        return $this->porter;
    }

    public function setPorter(string $porter): self
    {
        $this->porter = $porter;

        return $this;
    }


}
