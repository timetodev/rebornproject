<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TwitterLikes
 *
 * @ORM\Table(name="twitter_likes", indexes={@ORM\Index(name="FK_twitter_likes_twitter_tweets", columns={"tweetId"}), @ORM\Index(name="FK_twitter_likes_twitter_accounts", columns={"authorId"})})
 * @ORM\Entity
 */
class TwitterLikes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TwitterTweets
     *
     * @ORM\ManyToOne(targetEntity="TwitterTweets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tweetId", referencedColumnName="id")
     * })
     */
    private $tweetid;

    /**
     * @var \TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="TwitterAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="authorId", referencedColumnName="id")
     * })
     */
    private $authorid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTweetid(): ?TwitterTweets
    {
        return $this->tweetid;
    }

    public function setTweetid(?TwitterTweets $tweetid): self
    {
        $this->tweetid = $tweetid;

        return $this;
    }

    public function getAuthorid(): ?TwitterAccounts
    {
        return $this->authorid;
    }

    public function setAuthorid(?TwitterAccounts $authorid): self
    {
        $this->authorid = $authorid;

        return $this;
    }


}
