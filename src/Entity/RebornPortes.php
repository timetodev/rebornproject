<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RebornPortes
 *
 * @ORM\Table(name="reborn_portes")
 * @ORM\Entity
 */
class RebornPortes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="appartenance", type="string", length=255, nullable=false)
     */
    private $appartenance;

    /**
     * @var int
     *
     * @ORM\Column(name="fermeture", type="integer", nullable=false, options={"default"="1"})
     */
    private $fermeture = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAppartenance(): ?string
    {
        return $this->appartenance;
    }

    public function setAppartenance(string $appartenance): self
    {
        $this->appartenance = $appartenance;

        return $this;
    }

    public function getFermeture(): ?int
    {
        return $this->fermeture;
    }

    public function setFermeture(int $fermeture): self
    {
        $this->fermeture = $fermeture;

        return $this;
    }


}
