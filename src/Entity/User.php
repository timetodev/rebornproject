<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *  fields={"email"},
 *  message="L'email que vous avez indiqué est déjà utilisé"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre mot de passe doit contenir plus de 8 caracter")
     */
    private $password;
    
    /**
     * @Assert\EqualTo(propertyPath="password", message="Votre mot de passe ne correspond pas au mot de passe taper ci-dessus")
     */
    public $confirm_password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

      /**
     * @var json
     *
     * @ORM\Column(name="roles", type="json", nullable=true)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $steamHex;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $whitelisted;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireWhitelist;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tentativeWhitelist;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

   

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    public function __toString()
    {
        return $this->getUsername();
    }
    public function eraseCredentials(){}
    public function getSalt(){}

    public function getSteamHex(): ?string
    {
        return $this->steamHex;
    }

    public function setSteamHex(?string $steamHex): self
    {
        $this->steamHex = $steamHex;

        return $this;
    }

    public function getWhitelisted(): ?bool
    {
        return $this->whitelisted;
    }

    public function setWhitelisted(?bool $whitelisted): self
    {
        $this->whitelisted = $whitelisted;

        return $this;
    }

    public function getCommentaireWhitelist(): ?string
    {
        return $this->commentaireWhitelist;
    }

    public function setCommentaireWhitelist(?string $commentaireWhitelist): self
    {
        $this->commentaireWhitelist = $commentaireWhitelist;

        return $this;
    }

    public function getTentativeWhitelist(): ?int
    {
        return $this->tentativeWhitelist;
    }

    public function setTentativeWhitelist(?int $tentativeWhitelist): self
    {
        $this->tentativeWhitelist = $tentativeWhitelist;

        return $this;
    }

    
}
