<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArmesJoueurs
 *
 * @ORM\Table(name="armes_joueurs")
 * @ORM\Entity
 */
class ArmesJoueurs
{
    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $identifier;

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_arme", type="string", length=255, nullable=false)
     */
    private $nomArme;

    /**
     * @var string
     *
     * @ORM\Column(name="model_arme", type="string", length=60, nullable=false)
     */
    private $modelArme;

    /**
     * @var int
     *
     * @ORM\Column(name="hash_arme", type="integer", nullable=false)
     */
    private $hashArme;

    /**
     * @var int
     *
     * @ORM\Column(name="munitions_arme", type="integer", nullable=false)
     */
    private $munitionsArme;

    /**
     * @var int
     *
     * @ORM\Column(name="teinte_arme", type="integer", nullable=false)
     */
    private $teinteArme;

    /**
     * @var int
     *
     * @ORM\Column(name="prix_arme", type="integer", nullable=false)
     */
    private $prixArme;

    /**
     * @var int
     *
     * @ORM\Column(name="type_arme", type="integer", nullable=false)
     */
    private $typeArme;

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomArme(): ?string
    {
        return $this->nomArme;
    }

    public function setNomArme(string $nomArme): self
    {
        $this->nomArme = $nomArme;

        return $this;
    }

    public function getModelArme(): ?string
    {
        return $this->modelArme;
    }

    public function setModelArme(string $modelArme): self
    {
        $this->modelArme = $modelArme;

        return $this;
    }

    public function getHashArme(): ?int
    {
        return $this->hashArme;
    }

    public function setHashArme(int $hashArme): self
    {
        $this->hashArme = $hashArme;

        return $this;
    }

    public function getMunitionsArme(): ?int
    {
        return $this->munitionsArme;
    }

    public function setMunitionsArme(int $munitionsArme): self
    {
        $this->munitionsArme = $munitionsArme;

        return $this;
    }

    public function getTeinteArme(): ?int
    {
        return $this->teinteArme;
    }

    public function setTeinteArme(int $teinteArme): self
    {
        $this->teinteArme = $teinteArme;

        return $this;
    }

    public function getPrixArme(): ?int
    {
        return $this->prixArme;
    }

    public function setPrixArme(int $prixArme): self
    {
        $this->prixArme = $prixArme;

        return $this;
    }

    public function getTypeArme(): ?int
    {
        return $this->typeArme;
    }

    public function setTypeArme(int $typeArme): self
    {
        $this->typeArme = $typeArme;

        return $this;
    }


}
