<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoffreFortEntreprise
 *
 * @ORM\Table(name="coffre_fort_entreprise")
 * @ORM\Entity
 */
class CoffreFortEntreprise
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=25, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=25, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="x", type="string", length=25, nullable=false)
     */
    private $x;

    /**
     * @var string
     *
     * @ORM\Column(name="y", type="string", length=25, nullable=false)
     */
    private $y;

    /**
     * @var string
     *
     * @ORM\Column(name="z", type="string", length=25, nullable=false)
     */
    private $z;

    /**
     * @var string
     *
     * @ORM\Column(name="solde", type="string", length=25, nullable=false)
     */
    private $solde;

    /**
     * @var string
     *
     * @ORM\Column(name="solde_sale", type="string", length=25, nullable=false)
     */
    private $soldeSale;

    /**
     * @var string
     *
     * @ORM\Column(name="lasttransfert", type="string", length=25, nullable=false)
     */
    private $lasttransfert;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=25, nullable=false)
     */
    private $identifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="job", type="string", length=25, nullable=true)
     */
    private $job;

    /**
     * @var string|null
     *
     * @ORM\Column(name="job2", type="string", length=25, nullable=true)
     */
    private $job2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="job3", type="string", length=25, nullable=true)
     */
    private $job3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="boss1", type="string", length=25, nullable=true)
     */
    private $boss1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="boss2", type="string", length=25, nullable=true)
     */
    private $boss2;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getX(): ?string
    {
        return $this->x;
    }

    public function setX(string $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getY(): ?string
    {
        return $this->y;
    }

    public function setY(string $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getZ(): ?string
    {
        return $this->z;
    }

    public function setZ(string $z): self
    {
        $this->z = $z;

        return $this;
    }

    public function getSolde(): ?string
    {
        return $this->solde;
    }

    public function setSolde(string $solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getSoldeSale(): ?string
    {
        return $this->soldeSale;
    }

    public function setSoldeSale(string $soldeSale): self
    {
        $this->soldeSale = $soldeSale;

        return $this;
    }

    public function getLasttransfert(): ?string
    {
        return $this->lasttransfert;
    }

    public function setLasttransfert(string $lasttransfert): self
    {
        $this->lasttransfert = $lasttransfert;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(?string $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getJob2(): ?string
    {
        return $this->job2;
    }

    public function setJob2(?string $job2): self
    {
        $this->job2 = $job2;

        return $this;
    }

    public function getJob3(): ?string
    {
        return $this->job3;
    }

    public function setJob3(?string $job3): self
    {
        $this->job3 = $job3;

        return $this;
    }

    public function getBoss1(): ?string
    {
        return $this->boss1;
    }

    public function setBoss1(?string $boss1): self
    {
        $this->boss1 = $boss1;

        return $this;
    }

    public function getBoss2(): ?string
    {
        return $this->boss2;
    }

    public function setBoss2(?string $boss2): self
    {
        $this->boss2 = $boss2;

        return $this;
    }


}
