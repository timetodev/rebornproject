<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoffreFortJoueur
 *
 * @ORM\Table(name="coffre_fort_joueur")
 * @ORM\Entity
 */
class CoffreFortJoueur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=25, nullable=false, options={"default"="dispo"})
     */
    private $identifier = 'dispo';

    /**
     * @var string
     *
     * @ORM\Column(name="solde", type="string", length=25, nullable=false)
     */
    private $solde;

    /**
     * @var string
     *
     * @ORM\Column(name="solde_sale", type="string", length=25, nullable=false)
     */
    private $soldeSale;

    /**
     * @var string
     *
     * @ORM\Column(name="x", type="string", length=55, nullable=false)
     */
    private $x;

    /**
     * @var string
     *
     * @ORM\Column(name="y", type="string", length=55, nullable=false)
     */
    private $y;

    /**
     * @var string
     *
     * @ORM\Column(name="z", type="string", length=55, nullable=false)
     */
    private $z;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getSolde(): ?string
    {
        return $this->solde;
    }

    public function setSolde(string $solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getSoldeSale(): ?string
    {
        return $this->soldeSale;
    }

    public function setSoldeSale(string $soldeSale): self
    {
        $this->soldeSale = $soldeSale;

        return $this;
    }

    public function getX(): ?string
    {
        return $this->x;
    }

    public function setX(string $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getY(): ?string
    {
        return $this->y;
    }

    public function setY(string $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getZ(): ?string
    {
        return $this->z;
    }

    public function setZ(string $z): self
    {
        $this->z = $z;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
