<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventaireJoueurs
 *
 * @ORM\Table(name="inventaire_joueurs", indexes={@ORM\Index(name="item_id", columns={"item_id"})})
 * @ORM\Entity
 */
class InventaireJoueurs
{
    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $itemId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="genre", type="integer", nullable=true)
     */
    private $genre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    public function getGenre(): ?int
    {
        return $this->genre;
    }

    public function setGenre(?int $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
