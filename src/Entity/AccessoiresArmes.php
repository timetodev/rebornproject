<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessoiresArmes
 *
 * @ORM\Table(name="accessoires_armes")
 * @ORM\Entity
 */
class AccessoiresArmes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_arme", type="string", length=255, nullable=false)
     */
    private $nomArme;

    /**
     * @var string
     *
     * @ORM\Column(name="model_accessoire", type="string", length=60, nullable=false)
     */
    private $modelAccessoire;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_arme", type="string", length=60, nullable=false)
     */
    private $hashArme;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_acce", type="string", length=255, nullable=false)
     */
    private $hashAcce;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getNomArme(): ?string
    {
        return $this->nomArme;
    }

    public function setNomArme(string $nomArme): self
    {
        $this->nomArme = $nomArme;

        return $this;
    }

    public function getModelAccessoire(): ?string
    {
        return $this->modelAccessoire;
    }

    public function setModelAccessoire(string $modelAccessoire): self
    {
        $this->modelAccessoire = $modelAccessoire;

        return $this;
    }

    public function getHashArme(): ?string
    {
        return $this->hashArme;
    }

    public function setHashArme(string $hashArme): self
    {
        $this->hashArme = $hashArme;

        return $this;
    }

    public function getHashAcce(): ?string
    {
        return $this->hashAcce;
    }

    public function setHashAcce(string $hashAcce): self
    {
        $this->hashAcce = $hashAcce;

        return $this;
    }


}
