<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VetementsJoueurs
 *
 * @ORM\Table(name="vetements_joueurs")
 * @ORM\Entity
 */
class VetementsJoueurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_tenue", type="string", length=255, nullable=false)
     */
    private $nomTenue;

    /**
     * @var string
     *
     * @ORM\Column(name="donnees", type="string", length=2000, nullable=false, options={"default"="[]"})
     */
    private $donnees = '[]';

    /**
     * @var string
     *
     * @ORM\Column(name="porter", type="string", length=255, nullable=false, options={"default"="non"})
     */
    private $porter = 'non';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getNomTenue(): ?string
    {
        return $this->nomTenue;
    }

    public function setNomTenue(string $nomTenue): self
    {
        $this->nomTenue = $nomTenue;

        return $this;
    }

    public function getDonnees(): ?string
    {
        return $this->donnees;
    }

    public function setDonnees(string $donnees): self
    {
        $this->donnees = $donnees;

        return $this;
    }

    public function getPorter(): ?string
    {
        return $this->porter;
    }

    public function setPorter(string $porter): self
    {
        $this->porter = $porter;

        return $this;
    }


}
