<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClefsJoueurs
 *
 * @ORM\Table(name="clefs_joueurs")
 * @ORM\Entity
 */
class ClefsJoueurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicule_plaque", type="string", length=60, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $vehiculePlaque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=true, options={"default"="Citizen"})
     */
    private $prenom = 'Citizen';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=true, options={"default"="Citizen"})
     */
    private $nom = 'Citizen';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicule_model", type="string", length=60, nullable=true)
     */
    private $vehiculeModel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function getVehiculePlaque(): ?string
    {
        return $this->vehiculePlaque;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getVehiculeModel(): ?string
    {
        return $this->vehiculeModel;
    }

    public function setVehiculeModel(?string $vehiculeModel): self
    {
        $this->vehiculeModel = $vehiculeModel;

        return $this;
    }


}
