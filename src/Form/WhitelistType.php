<?php

namespace App\Form;

use App\Entity\Whitelist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class WhitelistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('steamId')
            ->add('nom')
            ->add('prenom')
            ->add('q1', TextareaType::class)
            ->add('q2', TextareaType::class)
            ->add('q3', TextareaType::class)
            ->add('q4', TextareaType::class)
            ->add('q5', TextareaType::class)
            ->add('background', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Whitelist::class,
        ]);
    }
}
