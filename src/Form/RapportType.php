<?php

namespace App\Form;

use App\Entity\Rapport;
use App\Form\PoliceTagsType;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class RapportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('agent')
            ->add('content',CKEditorType::class)
            ->add('categorie', ChoiceType::class, [
                'choices'  => [
                    'Rapport de Police' => "Rapport de police",
                    "Rapport D'enquête" => "Rapport D'enquête",
                ]])
            ->add('date', DateType::class)
            ->add('time', TimeType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rapport::class,
        ]);
    }
}
